import {useState, useEffect} from 'react';

import {catalog} from '../@types/catalog';

export default function useCatalog(catalogUri: string | undefined): catalog | undefined {
  const [catalog, setCatalog] = useState<catalog>();
  useEffect(() => {
    if (catalog) {
      return;
    }
    if (! catalogUri) {
      return;
    }
    fetch(catalogUri)
      .then(res => res.json())
      .then(data => setCatalog(data));
  }, [catalogUri, catalog]);

  return catalog;
}
