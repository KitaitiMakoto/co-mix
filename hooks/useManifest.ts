import {useState, useEffect} from 'react';

import {manifest} from '../@types/manifest';

export default function useManifest(manifestUri: string | undefined): manifest | undefined {
  const [manifest, setManifest] = useState<manifest>();
  useEffect(() => {
    if (manifest) {
      return;
    }
    if (! manifestUri) {
      return;
    }
    fetch(manifestUri)
      .then(res => res.json())
      .then(data => setManifest(data));
  }, [manifestUri, manifest]);

  return manifest;
}
