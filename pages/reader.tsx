import Head from 'next/head'
import {useRouter} from 'next/router';

import {Box} from '@chakra-ui/react';

import useManifest from '../hooks/useManifest';
import App from '../components/Reader';

export default function Reader() {
  const router = useRouter();
  const manifestUri = Array.isArray(router.query.manifest) ? router.query.manifest[0] : router.query.manifest;
  const manifest = useManifest(manifestUri);

  if (! manifest) {
    return <p>Loading...</p>;
  }

  return (
    <div>
      <Head>
        <title>{manifest.metadata.title}</title>
      </Head>

      <main>
        <h1>{manifest.metadata.title}</h1>
        <Box w="80%" mx="auto">
          <App readingOrder={manifest.readingOrder} />
        </Box>
      </main>
    </div>
  );
}
