import {useRouter} from 'next/router';

import useManifest from '../hooks/useManifest';
import App from '../components/Writer';

export default function Writer() {
  const router = useRouter();
  const manifestUri = Array.isArray(router.query.manifest) ? router.query.manifest[0] : router.query.manifest;
  
  const manifest = useManifest(manifestUri);

  if (! manifest) {
    return <p>Loading...</p>;
  }

  return (
    <main>
      <h1>{manifest.metadata.title}</h1>
      <App readingOrder={manifest.readingOrder} />
    </main>
  );
}
