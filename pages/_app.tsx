import '../styles/globals.css'
import type { AppProps } from 'next/app'
import {useRouter} from 'next/router';
import {ChakraProvider} from '@chakra-ui/react';

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  if (router.query.env === 'development') {
    document.documentElement.style.backgroundColor = 'white';
    document.documentElement.style.opacity = '1';
    document.body.style.backgroundColor = 'white';
    document.body.style.opacity = '1';
  }

  return (
    <ChakraProvider>
      <Component {...pageProps} />
    </ChakraProvider>
  );
}
export default MyApp
