import {Button, Center} from '@chakra-ui/react';

type Props = {
  onClick: () => void
};

export default function StartPage({onClick}: Props) {
  return (
    <Center
      position="absolute"
      top="0"
      left="0"
      width="full"
      height="full"
      bgColor="rgba(0, 0, 0, 0.5)"
    >
      <Button type="button" onClick={onClick}>開始（音が鳴ります）</Button>
    </Center>
  );
}
