import {useState, useCallback} from 'react';
import {VStack, Textarea} from '@chakra-ui/react';

import Controls from './Controls';

export default function Editor() {
  const [content, setContent] = useState('');

  const speak = useCallback(() => {
    speechSynthesis.cancel();
    const utt = new SpeechSynthesisUtterance(content);
    utt.rate = 1.5;
    speechSynthesis.speak(utt);
  }, [content]);

  const stop = useCallback(() => {
    speechSynthesis.cancel();
  }, []);

  const onKeyDown = useCallback(event => {
    if ((event.ctrlKey || event.metaKey) && event.key === 'p') {
      event.preventDefault();
      speak();
    } else if ((event.ctrlKey || event.metaKey) && event.key === 's') {
      event.preventDefault();
      stop();
    }
  }, [speak, stop]);

  return (
    <VStack h="full">
      <Controls flex="0 0 auto" onStart={speak} onStop={stop} />
      <Textarea
        flex="1 1 auto"
        h="full"
        lineHeight="1.8"
        value={content}
        onChange={event => setContent(event.currentTarget.value)}
        onKeyDown={onKeyDown}
      />
    </VStack>
  );
}
