import {HStack, Button, Kbd, StackProps} from '@chakra-ui/react';

type Props = StackProps & {
  onStart: () => void,
  onStop: () => void,
}

export default function Controls({onStart, onStop, ...stackProps}: Props) {
  return (
    <HStack {...stackProps}>
      <Button type="button" onClick={onStart}>再生（<Kbd>Ctrl</Kbd><Kbd>p</Kbd>）</Button>
      <Button type="button" onClick={onStop}>停止（<Kbd>Ctrl</Kbd><Kbd>s</Kbd>）</Button>
    </HStack>
  );
}
