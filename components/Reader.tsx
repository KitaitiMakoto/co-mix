import {useState} from 'react';
import {chakra, Box} from '@chakra-ui/react';

import {Resource} from '../@types/manifest';
import Page from './Page';
import StartPage from './StartPage';

type Props = {
  readingOrder: Resource[]
};

export default function Reader({readingOrder}: Props) {
  const [speakable, setSpeakable] = useState(false);
  const [current, setCurrent] = useState<number>(0);
  const resource = readingOrder[current];
  const nextResource = readingOrder[current + 1];

  if (! resource) {
    return <div>FIXME: End of manga</div>;
  }

  if (nextResource) {
    // preload
    const image = new Image();
    image.src = nextResource.href;
  }

  return (
    <Box>
      <Box position="relative">
        {speakable || <StartPage onClick={() => setSpeakable(true)} />}
        <Page res={resource} speakable={speakable} onClick={() => setCurrent(prev => prev + 1)} />
      </Box>
    </Box>
  );
}
