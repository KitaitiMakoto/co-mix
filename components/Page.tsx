import {useState, useEffect, useRef, MouseEvent} from 'react';
import {chakra, Box, BoxProps} from '@chakra-ui/react';

import {Resource, Content} from '../@types/manifest';

type Props = BoxProps & {
  res: Resource,
  speakable: boolean,
  onClick?: (event: MouseEvent) => void
};

export default function Page({res, speakable, onClick, ...boxProps}: Props) {
  const [imageLoaded, setImageLoaded] = useState(false);
  const [ended, setEnded] = useState(false);
  const buttonRef = useRef<HTMLButtonElement>(null);

  useEffect(() => {
    if (! speakable) {
      return;
    }
    setEnded(false);
    startContents(res.alt).then(() => setEnded(true));
  }, [res.alt, speakable]);
  useEffect(() => {
    if (! buttonRef.current) {
      return;
    }
    const button = buttonRef.current;
    if (ended) {
      button.focus();
    } else {
      button.blur();
    }
  }, [buttonRef, ended]);
  const onImageLoaded = () => setImageLoaded(true);
  const buttonDisabled = imageLoaded && !ended;

  return (
    <chakra.button
      ref={buttonRef}
      type="button"
      disabled={buttonDisabled}
      aria-label="次へ"
      _focus={{outline: 'solid'}}
      onClick={onClick}
    >
      <Box {...boxProps} w="full">
        <chakra.img src={res.href} onLoad={onImageLoaded} onError={onImageLoaded} w="full" />
      </Box>
    </chakra.button>
  );
}

async function startContents(contents: Content[] | undefined) {
  await new Promise(resolve => setTimeout(resolve, 1000)); // waiting for browser announcing page title
  for (const content of (contents ?? [])) {
    await startContent(content);
  }
}

function startContent(content: Content) {
  return new Promise<SpeechSynthesisEvent | void>(resolve => {
    if (content.text) {
      const utterance = new SpeechSynthesisUtterance(content.text);
      utterance.rate = 1.5;
      utterance.onend = resolve;
      speechSynthesis.speak(utterance);
    } else if (content.silence) {
      setTimeout(resolve, content.silence);
    } else {
      resolve();
    }
  });
}
