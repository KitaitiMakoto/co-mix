import React from 'react';
import {Box, Center} from '@chakra-ui/react';

import {manifest} from '../@types/manifest';

type Props = {
  manifest: manifest
};

export default function MangaLink({manifest}: Props) {
  const link = manifest.links.find(link => link.rel === 'self');

  return (
    <Box
      borderRadius="base"
      boxShadow="base"
    >
      {link ?
        <a href={`/reader?manifest=${encodeURIComponent(link.href)}`}>
          <Center h={['45vw', '45vw', '30vw']} p="4">{manifest.metadata.title}</Center>
        </a> :
        <Center h="48" p="4">Error!</Center>}
    </Box>
  );
}
