import {Grid, GridItem} from '@chakra-ui/react';

import {Resource} from '../@types/manifest';
import Editor from './Editor';

type Props = {
  readingOrder: Resource[]
};

export default function Writer({readingOrder}: Props) {
  return (
    <Grid templateColumns={['1fr', '1fr', '1fr 1fr']} w="80%" rowGap="8" columnGap="4" mx="auto">
      {readingOrder.map((resource, i) => (
        <>
          <GridItem key={`${i}-image`}><img src={resource.href} /></GridItem>
          <GridItem key={`${i}-content`}><Editor /></GridItem>
        </>
      ))}
    </Grid>
  );
}
