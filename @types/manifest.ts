export type manifest = {
  metadata: {
    title: string
  },
  links: Link[],
  readingOrder: Resource[],
  resources?: Resource[]
};

export type Resource = {
  href: string,
  type: string,
  rel?: string,
  title?: string,
  alt?: Content[]
};

export type Link = {
  rel: string,
  href: string,
  type?: string
};

export type Content = {
  text?: string,
  silence?: number
};
