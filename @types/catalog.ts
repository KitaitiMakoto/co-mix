import {manifest as Publication} from './manifest';

export type catalog = {
  publications: Publication[]
};
